package org.amicofragile.learning.tchain.clockapi

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = arrayOf(ClockApiApplication::class))
open class ClockApiApplication {
	@Bean
	open fun clock() : Clock = SystemClock()
}

fun main(args: Array<String>) {
	runApplication<ClockApiApplication>(*args)
}
