package org.amicofragile.learning.tchain.clockapi

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("")
class HomeController {
    @RequestMapping(method = [RequestMethod.GET])
    fun now(): Echo {
        return Echo("Welcome to clock-api app; try GET /api/clock")
    }
}

data class Echo(val text: String)